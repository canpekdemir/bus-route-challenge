package com.canpekdemir.bus.route.api.controller;

import com.canpekdemir.bus.route.api.model.response.BusRouteResponse;
import com.canpekdemir.bus.route.api.service.BusRouteService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class BusRouteController {

    private BusRouteService busRouteService;

    public BusRouteController(BusRouteService busRouteService) {
        this.busRouteService = busRouteService;
    }

    @GetMapping("/api/direct")
    @ResponseStatus(HttpStatus.OK)
    public BusRouteResponse retrieve(@RequestParam(value = "dep_sid") int departureStationId,
                                     @RequestParam(value = "arr_sid") int arrivalStationId) {
        boolean directBusRoute = busRouteService.checkRoute(departureStationId, arrivalStationId);
        return new BusRouteResponse(departureStationId,arrivalStationId,directBusRoute);
    }
}
