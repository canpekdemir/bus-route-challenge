package com.canpekdemir.bus.route.api.exception;

public class BusRouteException extends RuntimeException {

    public BusRouteException(String message) {
        super(message);
    }
}