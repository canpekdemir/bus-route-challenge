package com.canpekdemir.bus.route.api.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusRouteResponse {

    @JsonProperty("dep_sid")
    private int departureId;

    @JsonProperty("arr_sid")
    private int arrivalId;

    @JsonProperty("direct_bus_route")
    private boolean directBusRoute;

    public BusRouteResponse(int departureId, int arrivalId, boolean directBusRoute) {
        this.departureId = departureId;
        this.arrivalId = arrivalId;
        this.directBusRoute = directBusRoute;
    }

    public BusRouteResponse() {
    }

    public int getDepartureId() {
        return departureId;
    }

    public void setDepartureId(int departureId) {
        this.departureId = departureId;
    }

    public int getArrivalId() {
        return arrivalId;
    }

    public void setArrivalId(int arrivalId) {
        this.arrivalId = arrivalId;
    }

    public boolean isDirectBusRoute() {
        return directBusRoute;
    }

    public void setDirectBusRoute(boolean directBusRoute) {
        this.directBusRoute = directBusRoute;
    }
}
