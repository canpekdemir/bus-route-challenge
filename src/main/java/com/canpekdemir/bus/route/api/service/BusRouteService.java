package com.canpekdemir.bus.route.api.service;

import com.canpekdemir.bus.route.api.exception.BusRouteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
public class BusRouteService {

    private static final Logger logger = LoggerFactory.getLogger(BusRouteService.class);

    @Value("${data.file.path}")
    private String dataFilePath;

    private LineSplitter lineSplitter;

    public BusRouteService(LineSplitter lineSplitter) {
        this.lineSplitter = lineSplitter;
    }

    public boolean checkRoute(int departureStationId, int arrivalStationId) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(dataFilePath))) {

            return bufferedReader.lines()
                    .skip(1)
                    .anyMatch(line -> {
                        List<Integer> stations = lineSplitter.splitAsIntegerList(line);
                        return checkRouteExistsInStationList(departureStationId, arrivalStationId, stations);
                    });

        } catch (IOException e) {
            logger.error("An IO Exception occured while reading example file", e);
            throw new BusRouteException("data file read exception");
        }
    }

    public boolean checkRouteExistsInStationList(int departureStationId, int arrivalStationId, List<Integer> stations) {
        return stations.contains(departureStationId) && stations.contains(arrivalStationId);
    }
}
