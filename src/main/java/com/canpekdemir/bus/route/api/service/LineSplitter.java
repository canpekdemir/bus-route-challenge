package com.canpekdemir.bus.route.api.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LineSplitter {

    private static final String LINE_SPLIT_PATTERN = " ";

    public List<Integer> splitAsIntegerList(String line) {
        if(StringUtils.isBlank(line)){
            return new ArrayList<>();
        }
        return Arrays.stream(line.split(LINE_SPLIT_PATTERN)).map(Integer::valueOf).collect(Collectors.toList());
    }
}
