package com.canpekdemir.bus.route.api.controller;

import com.canpekdemir.bus.route.api.model.response.BusRouteResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BusRouteControllerFunctionalTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Value("${local.server.port}")
    private int serverPort;

    @Test
    public void should_return_true_when_route_is_found() {
        //given
        int departureStationId = 153;
        int arrivalStationId = 24;

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + serverPort + "/api/direct")
                .queryParam("dep_sid", departureStationId)
                .queryParam("arr_sid", arrivalStationId);

        //when
        ResponseEntity<BusRouteResponse> responseEntity = testRestTemplate
                .getForEntity(builder.build().encode().toUri(), BusRouteResponse.class);

        //then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        BusRouteResponse busRouteResponse = responseEntity.getBody();

        assertThat(busRouteResponse.getDepartureId()).isEqualTo(153);
        assertThat(busRouteResponse.getArrivalId()).isEqualTo(24);
        assertThat(busRouteResponse.isDirectBusRoute()).isTrue();
    }

    @Test
    public void should_return_false_when_route_is_not_found() {
        //given
        int departureStationId = 139;
        int arrivalStationId = 1254;

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + serverPort + "/api/direct")
                .queryParam("dep_sid", departureStationId)
                .queryParam("arr_sid", arrivalStationId);

        //when
        ResponseEntity<BusRouteResponse> responseEntity = testRestTemplate
                .getForEntity(builder.build().encode().toUri(), BusRouteResponse.class);

        //then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        BusRouteResponse busRouteResponse = responseEntity.getBody();

        assertThat(busRouteResponse.getDepartureId()).isEqualTo(139);
        assertThat(busRouteResponse.getArrivalId()).isEqualTo(1254);
        assertThat(busRouteResponse.isDirectBusRoute()).isFalse();
    }
}
