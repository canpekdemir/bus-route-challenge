package com.canpekdemir.bus.route.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BusRouteServiceTest {

    @InjectMocks
    BusRouteService busRouteService;

    @Mock
    LineSplitter lineSplitter;

    @Test
    public void should_return_true_when_route_exists_in_station_list() {
        //given
        int departureStationId= 1;
        int arrivalStationId = 7;
        List<Integer> stations = Arrays.asList(3,5,6,1,7);

        //when
        boolean result = busRouteService.checkRouteExistsInStationList(departureStationId, arrivalStationId, stations);

        //then
        assertThat(result).isTrue();
    }

    @Test
    public void should_return_false_when_route_not_exists_in_station_list() {
        //given
        int departureStationId= 1;
        int arrivalStationId = 7;
        List<Integer> stations = Arrays.asList(3,5,6,1);

        //when
        boolean result = busRouteService.checkRouteExistsInStationList(departureStationId, arrivalStationId, stations);

        //then
        assertThat(result).isFalse();
    }

}