package com.canpekdemir.bus.route.api.service;


import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LineSplitterTest {

    LineSplitter lineSplitter;

    @Before
    public void setUp() {
        lineSplitter = new LineSplitter();
    }

    @Test
    public void should_split_line() {
        //given
        String line = "1 2 3";

        //when
        List<Integer> values = lineSplitter.splitAsIntegerList(line);

        //then
        assertThat(values).hasSize(3).contains(1,2,3);
    }

    @Test
    public void should_return_empty_list_when_input_is_empty() {
        //given
        String line = "";

        //when
        List<Integer> values = lineSplitter.splitAsIntegerList(line);

        //then
        assertThat(values).hasSize(0);
    }
}